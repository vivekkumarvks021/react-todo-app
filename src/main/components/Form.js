import React, { Component } from "react";
import "./component.css";

class Form extends Component {
  render() {
    let { parentState, handleForm, handleTaskChange, handlePriorityChange } =
      this.props;
    return (
      <div className="block">
        <form onSubmit={handleForm}>
          <div className="form-control">
            <label>Todo</label>
            <input
              className="input"
              placeholder="Add Task"
              value={parentState.task}
              onChange={handleTaskChange}
            />
            <span className="error">{parentState.errors.task}</span>
          </div>
          <div className="form-control">
            <label>Priority</label>
            <select
              className="select"
              value={parentState.priority}
              onChange={handlePriorityChange}
            >
              <option>Select Priority</option>
              <option value="low">Low</option>
              <option value="medium">Medium</option>
              <option value="high">High</option>
            </select>
            <span className="error">{parentState.errors.priority}</span>
          </div>
          <button className="add-button" type="submit">
            Add
          </button>
        </form>
      </div>
    );
  }
}

export default Form;
