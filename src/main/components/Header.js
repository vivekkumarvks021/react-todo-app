import React, { Component } from "react";
import "./component.css";

class Header extends Component {
  render() {
    return (
      <div className="header">
        <h1>Todoey!</h1>
        <button className="clear-all" onClick={this.props.clearAll}>
          Clear All
        </button>
      </div>
    );
  }
}

export default Header;
