import React, { Component } from "react";
import Filter from "./components/Filter";
import Form from "./components/Form";
import Header from "./components/Header";
import TodoList from "./components/TodoList";
import "./components/component.css";

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      task: "",
      priority: "",
      todo_list: JSON.parse(localStorage.getItem("todo_list")),
      errors: {
        task: null,
        priority: null,
      },
    };
  }

  handleTaskChange = (event) => {
    this.setState({
      task: event.target.value,
    });
  };

  handlePriorityChange = (event) => {
    this.setState({
      priority: event.target.value,
    });
  };

  handleForm = (event) => {
    event.preventDefault();

    const { task, priority } = this.state;
    let validationErrors = {};

    validationErrors["task"] = this.validate("task", task);
    validationErrors["priority"] = this.validate("priority", priority);

    if (validationErrors.task !== "" || validationErrors.priority !== "") {
      this.setState({ errors: validationErrors });
      return;
    }

    this.setState(() => {
      let todo_list = [
        {
          task: task,
          priority: priority,
          mark: "uncompleted",
          id: Date.now(),
        },
        ...this.state?.todo_list,
      ];
      localStorage.setItem("todo_list", JSON.stringify(todo_list));

      return { todo_list };
    });
  };

  filter = (filterBy) => {
    let todoList = JSON.parse(localStorage.getItem("todo_list"));

    if (filterBy !== "all") {
      todoList = todoList.filter((data) => {
        return data.mark === filterBy;
      });
    }

    this.setState({
      todo_list: todoList,
    });
  };

  markAsDone = (index) => {
    this.setState((prevState) => {
      const { todo_list } = prevState;
      todo_list[index].mark = "completed";
      localStorage.setItem("todo_list", JSON.stringify(this.state.todo_list));
      return { todo_list: todo_list };
    });
  };

  deleteSingleTask = (id) => {
    let { todo_list } = this.state;

    todo_list = todo_list.filter((element) => {
      return element.id !== id;
    });

    this.setState(
      {
        todo_list: todo_list,
      },
      () => {
        localStorage.setItem("todo_list", JSON.stringify(todo_list));
      }
    );
  };

  clearAll = () => {
    this.setState(
      {
        todo_list: [],
      },
      () => {
        localStorage.setItem("todo_list", JSON.stringify([]));
      }
    );
  };

  validate = (name, value) => {
    if (!value) {
      return `${name} is required.`;
    } else {
      return "";
    }
  };

  render() {
    return (
      <main className="container">
        <Header clearAll={this.clearAll} />
        <Form
          parentState={this.state}
          handleTaskChange={this.handleTaskChange}
          handlePriorityChange={this.handlePriorityChange}
          handleForm={this.handleForm}
        />
        <Filter filter={this.filter} />
        <TodoList
          list={this.state.todo_list}
          markAsDone={this.markAsDone}
          deleteSingleTask={this.deleteSingleTask}
        />
      </main>
    );
  }
}

export default Main;
