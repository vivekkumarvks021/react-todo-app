import { Component } from "react";
import "./App.css";
import Main from "./main/Main";

class App extends Component {
  render() {
    return <Main className="app-bg" />;
  }
}

export default App;
